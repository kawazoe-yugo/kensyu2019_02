<!DOCTYPE html>
<?php
 include("./include/include_static.php");
?>
<html>
 <head>
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
     <link rel="stylesheet" href="./style.css">
     <meta charset="utf-8">
     <meta name="viewport" content="width=divice-width,initial-scale=1">
    <title>社員情報詳細</title>
 </head>
    <table class="table">
            <tr>
                <th class="top_name">社員名簿システム
                </th>
                    <td class="go_top_buttton">
                        |<a href="index.php">トップページへ戻る|</a>
                         <a href="entry01.php">新規社員登録画面</a>|
                    </td>
        </table>
<hr>
<script type="text/javascript">
 <!--
 function goEntry(){
     // 名前の空欄判定
     if(document.entry.namae.value == ""){
         alert("名前入力は必須です。");
         return false;
     }
     // 年齢の空欄判定
     if(document.entry.age.value == ""){
         alert("年齢入力は必須です。");
         return false;
     }
     // 年齢の最大最小値判定
     var temp = parseInt(document.entry.age.value);
     if(temp < 0 || temp > 99 ){
         alert("年齢は1～99で入力してください。");
         return false;
     }
     //出身地の選択判定
     if(document.entry.pref.value == ""){
          alert("出身地の選択は必須です。");
          return false;
      }
     if(window.confirm('登録を確定します。よろしいですか？')){
       //location.href = "./entry02.php?id=" + id;
       document.entry.submit();
     }
 }
 -->
 </script>
 <body>
        <!-- <a href="index.php"><p style="text-align:right">トップ画面</p></a>
        <a href="entry01.php"><p style="text-align:right">新規社員登録へ</p></a> -->

     <form method="post" action="entry02.php" name="entry">
        <table align="center"
          border-collapse: collapse;
          border:1px solid #c3c3c3;
          padding: 4px 12px;>
            <tr>
                <th>名前</th>
                    <td><input type="text" name="namae" value="" required>
                    </td>
            </tr>
            <tr>
                <th>出身地</th>
                    <td>
                        <select name="pref">
                            <?php
                               foreach ($prefecture_array as $each_id => $each_value){
                                   if($each_id == $result[0]['birthplace']){
                                       echo "<option value='" . $each_id . "' selected>" . $each_value . "</option>";
                                   }else{
                                       echo "<option value='" . $each_id . "'>" . $each_value . "</option>";
                                   }
                               }
                           ?>
                        </select>
                    </td>
            </tr>
            <tr>
                <th>性別</th>
                    <td><input type="radio" name="sex" value="2" checked="checked">男性
                        <input type="radio" name="sex" value="1">女性
                    </td>
            </tr>
            <tr>
                <th>年齢</th>
                    <td><input type="number" name="age" size="2" maxlength="3" value="" max="99" min="0">
                    </td>
            </tr>
            <tr>
                <th>所属部署</th>
                    <td><input type="radio" name="section" value="1" checked="checked">第一事業部
                        <input type="radio" name="section" value="2" >第二事業部
                        <input type="radio" name="section" value="3" >第三事業部
                        <input type="radio" name="section" value="4" >営業
                        <input type="radio" name="section" value="5" >総務
                        <input type="radio" name="section" value="6" >人事
                    </td>
            </tr>
            <tr>
                <th>役職</th>
                    <td><input type="radio" name="title" value="1" checked="checked">事業部長
                        <input type="radio" name="title" value="2">マネージャー
                        <input type="radio" name="title" value="3">リーダー
                        <input type="radio" name="title" value="4">メンバー
                        <input type="radio" name="title" value="5">チームリーダー
                    </td>
            </tr>
        </table>
        <table align="right">
                <td><input type="button" value="登録" onclick="goEntry();">
                    <input type="reset" name="kesu" value="リセットする">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>

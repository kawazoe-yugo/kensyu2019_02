<!DOCTYPE html>
<?php
include("./include/include_static.php");
// comon
// include
$DB_DSN = "mysql:host=localhost; dbname=kensyu_meibo; charset=utf8";
$DB_USER = "kensyu_login";
$DB_PW = "tZDHOaC29ZB5p5zC";
$pdo = new PDO($DB_DSN, $DB_USER, $DB_PW);
// $pdo = initDB();

// $query_str = "SELECT * FROM meibo";
// $query_str = "SELECT * FROM 'test_table' WHERE dish_mane LIKE '%の%' AND genre = 'おつまみ'";
$query_str = "SELECT m.name,m.ID, m.section, m.title, m.birthplace,m.sex, m.age
        FROM meibo AS m
        WHERE m.ID =" . $_GET['id'];

$sql = $pdo->prepare($query_str);
$sql->execute();
$result = $sql->fetchAll();

// echo $query_str;
//
// echo "<pre>";
//     var_dump($result);
// echo "</pre>";

?>
<html>
 <head>
  <link rel="stylesheet" href="style.css">
  <meta charset="utf-8">
  <meta name="viewport" content="width=divice-width,initial-scale=1">
    <title>社員情報詳細</title>
 </head>
 <script type="text/javascript">
 <!--
 function goUpDate(){
     // 名前の空欄判定
     if(document.update.namae.value == ""){
         alert("名前入力は必須です。");
         return false;
     }
     // 年齢の空欄判定
     if(document.update.age.value == ""){
         alert("年齢入力は必須です。");
         return false;
     }
     // 年齢の最大最小値判定
     var temp = parseInt(document.update.age.value);
     if(temp < 0 || temp > 99 ){
         alert("年齢は1～99で入力してください。");
         return false;
     }
     //出身地の選択判定
     if(document.update.pref.value == ""){
          alert("出身地の選択は必須です。");
          return false;
      }
     if(window.confirm('編集を確定します。よろしいですか？')){
       //location.href = "./entry_update02.php?id=" + id;
       document.update.submit();
     }
 }
 -->
 </script>
 <body>
     <table class="top_table">
         <tr>
             <th class="top_name">社員名簿システム
             </th>
                 <td class="go_top_buttton">
                     |<a href="index.php">トップページへ戻る|</a>
                      <a href="entry01.php">新規社員登録画面</a>|
                 </td>
     </table>
    <hr>
     <form method="POST" action="entry_update02.php" name="update">
         <table border="1" align="center">

            <tr>
                <th>社員ID</th>
                <td><?php echo  $result[0]['ID']?></td>
            </tr>
             <tr>
                 <th>名前</th>
                    <td><input type="text" name="namae" size="20" maxlength="18"
                        value="<?php echo  $result[0]['name'];?>"</td>
             </tr>
             <tr>
             <th>出身地</th>
                 <td>
                     <select name="pref">
                         <?php
                            foreach ($prefecture_array as $each_id => $each_value){
                                if($each_id == $result[0]['birthplace']){
                                    echo "<option value='" . $each_id . "' selected>" . $each_value . "</option>";
                                }else{
                                    echo "<option value='" . $each_id . "'>" . $each_value . "</option>";
                                }
                            }
                        ?>
                 </td>
             </tr>
             <tr>
                 <th>性別</th>
                 <td>
                     <?php
                        if($result[0]['sex'] == 2){
                            echo "<input type='radio' name='sex' value='2' checked> 男性
                                  <input type=radio name=sex value=1 > 女性";
                        }else{
                            echo "<input type='radio' name='sex' value='2' >男性
                                 <input type='radio' name='sex' value='1' checked>女性";
                        }
                    ?>
                 </td>
             </tr>
             <tr>
                 <th>年齢</th>
                 <td><input type="number" name="age" size="20" maxlength="18"
                     value="<?php echo  $result[0]['age'];?>"
                 </td>
             </tr>
             <tr>
                 <th>所属部署</th>
                 <td>
                     <input type="radio" name="section" value="1" <?php if($result[0]['section'] == "1"){echo "checked";}?>>第一事業部
                     <input type="radio" name="section" value="2" <?php if($result[0]['section'] == "2"){echo "checked";}?>>第二事業部
                     <input type="radio" name="section" value="3" <?php if($result[0]['section'] == "3"){echo "checked";}?>>第三事業部
                     <input type="radio" name="section" value="4" <?php if($result[0]['section'] == "4"){echo "checked";}?>>営業
                     <input type="radio" name="section" value="5" <?php if($result[0]['section'] == "5"){echo "checked";}?>>総務
                     <input type="radio" name="section" value="6" <?php if($result[0]['section'] == "6"){echo "checked";}?>>人事
                 </td>
             </tr>
             <tr>
                 <th>役職</th>
                 <td>
                      <input type="radio" name="title" value="1" <?php if($result[0]['title'] == "1"){echo "checked";}?>>事業部長
                      <input type="radio" name="title" value="2" <?php if($result[0]['title'] == "2"){echo "checked";}?>>マネージャー
                      <input type="radio" name="title" value="3" <?php if($result[0]['title'] == "3"){echo "checked";}?>>リーダー
                      <input type="radio" name="title" value="4" <?php if($result[0]['title'] == "4"){echo "checked";}?>>メンバー
                      <input type="radio" name="title" value="5" <?php if($result[0]['title'] == "5"){echo "checked";}?>>チームリーダー
                 </td>
             </tr>
        </table>
    <div class="entry_button">
      <input type="hidden" name="id" value="<?php echo $result[0]['ID'];?>">
      <input type="button"  value="登録" onclick="goUpDate();">
      <input type="reset"  name="delete" value="リセット">
    </div>
</form>
</head>
</html>
